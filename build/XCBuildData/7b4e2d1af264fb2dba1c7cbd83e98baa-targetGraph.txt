Target dependency graph (39 targets)
Firebase in Firebase, no dependencies
GoogleUtilities-NSData in GoogleUtilities, no dependencies
GULNSData in GoogleUtilities, depends on:
GoogleUtilities-NSData in GoogleUtilities (explicit)
FirebaseCoreInternal in Firebase, depends on:
GULNSData in GoogleUtilities (explicit)
FBLPromises in Promises, no dependencies
FBLPromises in Promises, depends on:
FBLPromises in Promises (explicit)
GoogleUtilities-Environment in GoogleUtilities, depends on:
FBLPromises in Promises (explicit)
GULEnvironment in GoogleUtilities, depends on:
GoogleUtilities-Environment in GoogleUtilities (explicit)
FBLPromises in Promises (explicit)
GoogleUtilities-Logger in GoogleUtilities, depends on:
GoogleUtilities-Environment in GoogleUtilities (explicit)
FBLPromises in Promises (explicit)
GULLogger in GoogleUtilities, depends on:
GoogleUtilities-Logger in GoogleUtilities (explicit)
GoogleUtilities-Environment in GoogleUtilities (explicit)
FBLPromises in Promises (explicit)
FirebaseCore in Firebase, depends on:
Firebase in Firebase (explicit)
FirebaseCoreInternal in Firebase (explicit)
GULNSData in GoogleUtilities (explicit)
GULEnvironment in GoogleUtilities (explicit)
GULLogger in GoogleUtilities (explicit)
GoogleUtilities-Reachability in GoogleUtilities, depends on:
GoogleUtilities-Environment in GoogleUtilities (explicit)
GoogleUtilities-Logger in GoogleUtilities (explicit)
FBLPromises in Promises (explicit)
GoogleUtilities-Network in GoogleUtilities, depends on:
GoogleUtilities-Environment in GoogleUtilities (explicit)
GoogleUtilities-Logger in GoogleUtilities (explicit)
GoogleUtilities-NSData in GoogleUtilities (explicit)
GoogleUtilities-Reachability in GoogleUtilities (explicit)
FBLPromises in Promises (explicit)
GoogleUtilities-AppDelegateSwizzler in GoogleUtilities, depends on:
GoogleUtilities-Environment in GoogleUtilities (explicit)
GoogleUtilities-Logger in GoogleUtilities (explicit)
GoogleUtilities-NSData in GoogleUtilities (explicit)
GoogleUtilities-Reachability in GoogleUtilities (explicit)
GoogleUtilities-Network in GoogleUtilities (explicit)
FBLPromises in Promises (explicit)
GULAppDelegateSwizzler in GoogleUtilities, depends on:
GoogleUtilities-AppDelegateSwizzler in GoogleUtilities (explicit)
GoogleUtilities-Environment in GoogleUtilities (explicit)
GoogleUtilities-Logger in GoogleUtilities (explicit)
GoogleUtilities-NSData in GoogleUtilities (explicit)
GoogleUtilities-Reachability in GoogleUtilities (explicit)
GoogleUtilities-Network in GoogleUtilities (explicit)
FBLPromises in Promises (explicit)
GTMSessionFetcherCore in GTMSessionFetcher, no dependencies
GTMSessionFetcherCore in GTMSessionFetcher, depends on:
GTMSessionFetcherCore in GTMSessionFetcher (explicit)
FirebaseAuth in Firebase, depends on:
Firebase in Firebase (explicit)
FirebaseCoreInternal in Firebase (explicit)
FirebaseCore in Firebase (explicit)
GULNSData in GoogleUtilities (explicit)
GULEnvironment in GoogleUtilities (explicit)
GULLogger in GoogleUtilities (explicit)
GULAppDelegateSwizzler in GoogleUtilities (explicit)
GTMSessionFetcherCore in GTMSessionFetcher (explicit)
FirebaseAuth in Firebase, depends on:
FirebaseAuth in Firebase (explicit)
Firebase in Firebase (explicit)
FirebaseCoreInternal in Firebase (explicit)
FirebaseCore in Firebase (explicit)
GULNSData in GoogleUtilities (explicit)
GULEnvironment in GoogleUtilities (explicit)
GULLogger in GoogleUtilities (explicit)
GULAppDelegateSwizzler in GoogleUtilities (explicit)
GTMSessionFetcherCore in GTMSessionFetcher (explicit)
leveldb in leveldb, no dependencies
leveldb in leveldb, depends on:
leveldb in leveldb (explicit)
nanopb in nanopb, no dependencies
nanopb in nanopb, depends on:
nanopb in nanopb (explicit)
abseil in abseil, no dependencies
abseil in abseil, depends on:
abseil in abseil (explicit)
openssl_grpc in BoringSSL-GRPC, no dependencies
openssl_grpc in BoringSSL-GRPC, depends on:
openssl_grpc in BoringSSL-GRPC (explicit)
gRPC-Core in gRPC, depends on:
abseil in abseil (explicit)
openssl_grpc in BoringSSL-GRPC (explicit)
gRPC-cpp in gRPC, depends on:
gRPC-Core in gRPC (explicit)
abseil in abseil (explicit)
openssl_grpc in BoringSSL-GRPC (explicit)
gRPC-cpp in gRPC, depends on:
gRPC-cpp in gRPC (explicit)
gRPC-Core in gRPC (explicit)
abseil in abseil (explicit)
openssl_grpc in BoringSSL-GRPC (explicit)
FirebaseFirestore in Firebase, depends on:
Firebase in Firebase (explicit)
FirebaseCoreInternal in Firebase (explicit)
FirebaseCore in Firebase (explicit)
GULNSData in GoogleUtilities (explicit)
GULEnvironment in GoogleUtilities (explicit)
GULLogger in GoogleUtilities (explicit)
leveldb in leveldb (explicit)
nanopb in nanopb (explicit)
abseil in abseil (explicit)
gRPC-cpp in gRPC (explicit)
FirebaseFirestoreTarget in Firebase, depends on:
Firebase in Firebase (explicit)
FirebaseCoreInternal in Firebase (explicit)
FirebaseCore in Firebase (explicit)
FirebaseFirestore in Firebase (explicit)
GULNSData in GoogleUtilities (explicit)
GULEnvironment in GoogleUtilities (explicit)
GULLogger in GoogleUtilities (explicit)
leveldb in leveldb (explicit)
nanopb in nanopb (explicit)
abseil in abseil (explicit)
gRPC-cpp in gRPC (explicit)
FirebaseFirestore in Firebase, depends on:
FirebaseFirestoreTarget in Firebase (explicit)
Firebase in Firebase (explicit)
FirebaseCoreInternal in Firebase (explicit)
FirebaseCore in Firebase (explicit)
FirebaseFirestore in Firebase (explicit)
GULNSData in GoogleUtilities (explicit)
GULEnvironment in GoogleUtilities (explicit)
GULLogger in GoogleUtilities (explicit)
leveldb in leveldb (explicit)
nanopb in nanopb (explicit)
abseil in abseil (explicit)
gRPC-cpp in gRPC (explicit)
FirebaseAppCheckInterop in Firebase, no dependencies
FirebaseAuthInterop in Firebase, no dependencies
FirebaseCoreExtension in Firebase, no dependencies
FirebaseStorage in Firebase, depends on:
FirebaseAppCheckInterop in Firebase (explicit)
FirebaseAuthInterop in Firebase (explicit)
Firebase in Firebase (explicit)
FirebaseCoreInternal in Firebase (explicit)
FirebaseCore in Firebase (explicit)
FirebaseCoreExtension in Firebase (explicit)
GULNSData in GoogleUtilities (explicit)
GULEnvironment in GoogleUtilities (explicit)
GULLogger in GoogleUtilities (explicit)
GTMSessionFetcherCore in GTMSessionFetcher (explicit)
FirebaseStorage in Firebase, depends on:
FirebaseStorage in Firebase (explicit)
FirebaseAppCheckInterop in Firebase (explicit)
FirebaseAuthInterop in Firebase (explicit)
Firebase in Firebase (explicit)
FirebaseCoreInternal in Firebase (explicit)
FirebaseCore in Firebase (explicit)
FirebaseCoreExtension in Firebase (explicit)
GULNSData in GoogleUtilities (explicit)
GULEnvironment in GoogleUtilities (explicit)
GULLogger in GoogleUtilities (explicit)
GTMSessionFetcherCore in GTMSessionFetcher (explicit)
Project in Project, depends on:
FirebaseAuth in Firebase (explicit)
FirebaseFirestore in Firebase (explicit)
FirebaseStorage in Firebase (explicit)