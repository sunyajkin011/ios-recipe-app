//
//  TyoeOfListView.swift
//  Project
//
//  Created by кирилл суняйкин on 19.01.2023.
//

import Foundation

enum TypeOfListView: String, CaseIterable{
    case General = "general"
    case Favorite = "favorite"
    case Filtered = "Brno-Bystrc"
}
