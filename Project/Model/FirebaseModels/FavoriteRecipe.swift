//
//  FavoriteRecipe.swift
//  Project
//
//  Created by кирилл суняйкин on 21.01.2023.
//

import Foundation
import FirebaseFirestore


struct FavoriteRecipe: Identifiable{

        var id: Int
        var userId: String
        var cookTimeMinutes: Int?
        var name: String
        var instractions: [String]?
        var thumbnailUrl: String
        var sections: [String]?
    
    
    
    var representation: [String: Any]{
        var repres = [String: Any]()
        repres["id"] = id
        repres["userId"] = userId
        repres["cookTimeMinutes"] = cookTimeMinutes
        repres["name"] = name
        repres["instractions"] = instractions
        repres["thumbnailUrl"] = thumbnailUrl
        repres["sections"] = sections
    
        return repres
    }
    
    
    init(id: Int,
         userId: String,
         cookTimeMinutes: Int?,
         name: String,
         instractions: [String]?,
         thumbnailUrl: String,
         sections: [String]?
    ){
        self.id = id
        self.userId = userId
        self.cookTimeMinutes = cookTimeMinutes
        self.name = name
        self.instractions = instractions
        self.thumbnailUrl = thumbnailUrl
        self.sections = sections
    }
    
    
        init?(doc: QueryDocumentSnapshot){
            let data = doc.data()
            
            guard let id = data["id"] as? Int else {return nil}
            guard let userId = data["userId"] as? String else {return nil}
           guard let cookTimeMinutes = data["cookTimeMinutes"] as? Int else {return nil}
            guard let name = data["name"] as? String else {return nil}
            //guard let instractions = data["instractions"] as? Array<String> else {return nil}
            guard let thumbnailUrl = data["thumbnailUrl"] as? String else {return nil}
           guard let sections = data["sections"] as? Array<String> else {return nil}
            
    
            self.id = id
            self.userId = userId
            self.cookTimeMinutes = cookTimeMinutes
            self.name = name
            //self.instractions = instractions
            self.thumbnailUrl = thumbnailUrl
            self.sections = sections
        }
}

