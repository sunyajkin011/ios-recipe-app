//
//  Recipe.swift
//  Project
//
//  Created by кирилл суняйкин on 21.01.2023.
//

import Foundation
import FirebaseFirestore


struct RecipeLocal{

        var id: Int
        var cookTimeMinutes: Int?
        var name: String
        var instructions: [String]?
        var thumbnailUrl: String
        var sections: [String]?
    
    
    
    var representation: [String: Any]{
        var repres = [String: Any]()
        repres["id"] = id
        repres["cookTimeMinutes"] = cookTimeMinutes
        repres["name"] = name
        repres["instructions"] = instructions
        repres["thumbnailUrl"] = thumbnailUrl
        repres["sections"] = sections
    
        return repres
    }
    
    
    init(id: Int,
         cookTimeMinutes: Int?,
         name: String,
         instructions: [String]?,
         thumbnailUrl: String,
         sections: [String]?
    ){
        self.id = id
        self.cookTimeMinutes = cookTimeMinutes
        self.name = name
        self.instructions = instructions
        self.thumbnailUrl = thumbnailUrl
        self.sections = sections
    }
    
    
        init?(doc: QueryDocumentSnapshot){
            let data = doc.data()
            
            guard let id = data["id"] as? Int else {return nil}
            guard let cookTimeMinutes = data["cookTimeMinutes"] as? Int else {return nil}
            guard let name = data["name"] as? String else {return nil}
            guard let instructions = data["instructions"] as? Array<String> else {return nil}
            guard let thumbnailUrl = data["thumbnailUrl"] as? String else {return nil}
            guard let sections = data["sections"] as? Array<String> else {return nil}
            
    
            self.id = id
            self.cookTimeMinutes = cookTimeMinutes
            self.name = name
            self.instructions = instructions
            self.thumbnailUrl = thumbnailUrl
            self.sections = sections
        }
}
