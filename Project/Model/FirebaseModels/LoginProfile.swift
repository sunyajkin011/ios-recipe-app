//
//  LoginProfile.swift
//  Project
//
//  Created by кирилл суняйкин on 21.01.2023.
//

import Foundation


struct LoginProfile: Identifiable{
    
    
    var id: String
    var name: String
    var surname: String
    var imageUrl: String
    var number: String
    var email: String
    
    
    var representation: [String: Any]{
        var repres = [String: Any]()
        repres["id"] = id
        repres["name"] = name
        repres["surname"] = surname
        repres["imageUrl"] = imageUrl
        repres["number"] = number
        repres["email"] = email
        
         return repres
    }
    
    init(id: String,
         name: String,
         surname: String,
         imageUrl: String,
         number: String,
         email: String){
        self.id = id
        self.name = name
        self.surname = surname
        self.imageUrl = imageUrl
        self.number = number
        self.email = email
    }
    

    
    
}
