//
//  TypeOfDetailView.swift
//  Project
//
//  Created by кирилл суняйкин on 19.01.2023.
//

import Foundation


enum TypeOfDetailView: String, CaseIterable{
    case FromGeneral = "from general view"
    case FromFavorite = "from favorite view"
    case FromFiltered = "from filtered view"
}
