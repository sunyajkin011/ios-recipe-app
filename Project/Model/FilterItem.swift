//
//  FilterItem.swift
//  Project
//
//  Created by кирилл суняйкин on 24.01.2023.
//

import Foundation


struct FilterItem: Identifiable, Codable{
    var id: Int
    var title: String
    var isSelected: Bool
}
