//
//  Recipe.swift
//  Project
//
//  Created by кирилл суняйкин on 24.12.2022.
//

import Foundation


struct Recipe: Codable{
    var id: Int
    var cook_time_minutes: Int?
    var name: String
    var instructions: [Instruction]?
    var thumbnail_url: String
    var sections: [Position]?
}

