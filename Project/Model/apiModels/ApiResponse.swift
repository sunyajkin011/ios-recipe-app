//
//  ApiResponce.swift
//  Project
//
//  Created by кирилл суняйкин on 24.12.2022.
//

import Foundation

struct  ApiResponse : Codable{
    var count: Int
    var results: [Recipe]
}
