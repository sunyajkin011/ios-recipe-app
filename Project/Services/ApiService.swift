//
//  ApiService.swift
//  Project
//
//  Created by кирилл суняйкин on 24.12.2022.
//

import Foundation


enum ApiResponseError: Error {
    case buildUrl
    case jsonDecoder
}

class ApiService  {

    
    func getAllRecipies(completion: @escaping (Result<ApiResponse, ApiResponseError>) -> Void) {
        let urlString = NSString(format: "https://tasty.p.rapidapi.com/recipes/list?from=0&size=40&tags=under_30_minutes") as String

        let request = NSMutableURLRequest(url: URL(string: urlString)!)
                    request.allHTTPHeaderFields = [
                        "X-RapidAPI-Key": "286fa27844msh16475c37a4ac26ep1b47ebjsnea58f4187b99",
                        "X-RapidAPI-Host": "tasty.p.rapidapi.com"
                    ]
                    request.httpMethod = "GET"
                    let session = URLSession.shared

                    let dataTask = session.dataTask(with: request as URLRequest) { (data, response, error) in

                        if let error = error {
                            completion(.failure(error as! ApiResponseError))
                            print("chyba")
                            print(error)
                        return
                        }
                        guard let data = data else {
                            print("Empty data")
                            return
                        }

                        do{
                         let decoder = JSONDecoder()
                         let codabledata = try decoder.decode(ApiResponse.self, from: data)
                            DispatchQueue.main.async {
                                print("succes")
                                completion(.success(codabledata))
                            }

                        } catch _ {

                           completion(.failure(.jsonDecoder))
                        }
                    }
        dataTask.resume()
            }
}

