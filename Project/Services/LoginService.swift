//
//  LoginService.swift
//  Project
//
//  Created by кирилл суняйкин on 21.01.2023.
//

import Foundation
import FirebaseAuth
import FirebaseStorage
import FirebaseFirestore


class LoginService{
    
    static let shared = LoginService()
    
    private init(){}
    
    private let auth = Auth.auth()
    
    var currentUser: User?{
        return auth.currentUser
    }
    
    
    func signUp(email:String, password: String, completion: @escaping (Result<User, Error>) -> ()){
        auth.createUser(withEmail: email, password: password){ result, error in
            if let result = result{
                let profileUser = LoginProfile(id: result.user.uid, name: "", surname: "", imageUrl: "", number: "", email: result.user.email!)
                FirebaseService.shared.setUser(user: profileUser){ resultDB in
                    switch resultDB {
                    case .success(_):
                        completion(.success(result.user))
                    case .failure(let error):
                        print(error)
                    }
                }
            } else if let error = error{
                completion(.failure(error))
            }
            
        }
    }
    func signIn(email:String, password: String, completion: @escaping (Result<User, Error>) -> ()){
        auth.signIn(withEmail: email, password: password){ result, error in
            
            if let result = result{
                completion(.success(result.user))
            } else if let error = error{
                completion(.failure(error))
            }
            
        }
        
    }
}
