//
//  FirebaseService.swift
//  Project
//
//  Created by кирилл суняйкин on 21.01.2023.
//

import Foundation
import FirebaseAuth
import FirebaseStorage
import FirebaseFirestore

class FirebaseService{
    
    static let shared = FirebaseService()
    private let db = Firestore.firestore()
    
    private init(){}
    
    private var recipeRef: CollectionReference{
        return db.collection("recipeLocal")
    }
    
    private var userRef: CollectionReference{
        return db.collection("user")
    }
    
    private var favoriteRecipeRef: CollectionReference{
        return db.collection("favoriteRecipe")
    }
    
    

    
    func getUser(completion: @escaping (Result<LoginProfile, Error>) ->()){
        userRef.document(LoginService.shared.currentUser!.uid).getDocument{ docSnapshot, error in
            
            guard let snap = docSnapshot else{ return }
            guard let data = snap.data() else{ return }
            
            guard let id = data["id"] as? String else {return}
            guard let name = data["name"] as? String else {return}
            guard let surname = data["surname"] as? String else {return}
            guard let email = data["email"] as? String else {return}
            guard let number = data["number"] as? String else {return}
            guard let imageUrl = data["imageUrl"] as? String else {return}
            
            
            let loginProfile = LoginProfile(id: id, name: name, surname: surname, imageUrl: imageUrl, number: number, email: email)
            
            completion(.success(loginProfile))
        }
    }
    
    
    func setUser(user: LoginProfile, completion: @escaping (Result<LoginProfile, Error>) ->()){
        
        userRef.document(user.id).setData(user.representation){
            error in
            if let error = error {
                completion(.failure(error))
            }else{
                completion(.success(user))
            }
        }
        
    }
    
    func setAllLocalRecipe(recipesLocal: [RecipeLocal], completion: @escaping (Result<RecipeLocal, Error>)->()){
        
        for res in recipesLocal{
            recipeRef.document(String(res.id)).setData(res.representation){
                error in
                if let error = error {
                                completion(.failure(error))
                            }else{
                                completion(.success(res))
                            }
            }
        }
    }
    
    
    func setFavoriteRecipe(recipeFavorite: FavoriteRecipe, completion: @escaping (Result<FavoriteRecipe, Error>)->()){
        
        
        favoriteRecipeRef.document(String(recipeFavorite.id)).setData(recipeFavorite.representation){
                error in
                if let error = error {
                                completion(.failure(error))
                            }else{
                                completion(.success(recipeFavorite))
                }
        }
    }
    
    func getFavoriteRecipes( completion: @escaping (Result<[FavoriteRecipe], Error>)->()){
               self.favoriteRecipeRef.getDocuments{ qSnap, error in
       
                   if let qSnap = qSnap{
                       var favoriteRecipes = [FavoriteRecipe]()
                       for doc in qSnap.documents{
                           if let favoriteRecipe = FavoriteRecipe(doc: doc){
                               if(favoriteRecipe.userId == LoginService.shared.currentUser!.uid){
                                   favoriteRecipes.append(favoriteRecipe)
                               }
                           }
                       }
                       completion(.success(favoriteRecipes))
                       print("\(favoriteRecipes.count)")
                   } else if let error = error{
                       completion(.failure(error))
            }
        }
    }
    
    func getAllLocalRecipes( completion: @escaping (Result<[RecipeLocal], Error>)->()){
               self.recipeRef.getDocuments{ qSnap, error in
       
                   if let qSnap = qSnap{
                       var allLocalRecipes = [RecipeLocal]()
                       for doc in qSnap.documents{
                           if let localRecipe = RecipeLocal(doc: doc){
                               allLocalRecipes.append(localRecipe)
                           }
                       }
                       completion(.success(allLocalRecipes))
                       print("\(allLocalRecipes.count)")
                   } else if let error = error{
                       completion(.failure(error))
            }
        }
    }
    
    
    func deleteItem(item: FavoriteRecipe){
        favoriteRecipeRef.document(String(item.id)).delete(){ error in
                if let error = error{
                    print(error.localizedDescription)
                }else{
                    print("delete success")
                }
            }
        }
    
    
    

}


