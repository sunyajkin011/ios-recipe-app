//
//  ApiServiceProtocol.swift
//  Project
//
//  Created by кирилл суняйкин on 24.12.2022.
//

import Foundation

protocol ApiServiceProtocol{
    func getAllRecipies() async -> Result<ApiResponse, ApiResponseError>
}
