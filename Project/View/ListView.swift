//
//  ListView.swift
//  Project
//
//  Created by кирилл суняйкин on 18.01.2023.
//

import SwiftUI

struct ListView: View {
    
    @StateObject var viewModel: ListViewModel
    
    var body: some View {
        ScrollView(.vertical, showsIndicators: false){
            LazyVGrid(columns: [GridItem(.adaptive(minimum: screen.width / 2.0))]){
                ForEach(viewModel.recipes, id: \.id){
                    recipe in
                    NavigationLink{
                        
                        let viewModel = DetailViewModel(recipe: recipe, type: "\(self.viewModel.type)" )
                        
                        DetailView(viewModel: viewModel)
                        
                    }label:{
                        RecipeField(recipe: recipe)
                        
                    }
                }
            }
            }.padding(10)
            .onAppear{
                self.viewModel.fetchData()
          }
            
    }
}

struct ListView_Previews: PreviewProvider {
    static var previews: some View {
        ListView(viewModel: ListViewModel(type: "main"))
    }
}
