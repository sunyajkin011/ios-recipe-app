//
//  ProfileView.swift
//  Project
//
//  Created by кирилл суняйкин on 19.01.2023.
//

import SwiftUI
import FirebaseStorage
import FirebaseFirestore

struct ProfileView: View {
    
    @State var isPickerPresented = false
    @State var selectedImage: UIImage?

    @StateObject var viewModel: ProfileViewModel
    
    var body: some View {
        VStack(){
        
            VStack{
                if selectedImage != nil {
                    Image(uiImage: selectedImage!)
                        .resizable()
                        .frame(width: 200 , height: 200)
                        .cornerRadius(70)
                        .padding(32)
                        .clipShape(Circle())
                        .onTapGesture {
                            isPickerPresented.toggle()
                        }.sheet(isPresented: $isPickerPresented){
                            ImagePicker(selectedImage: $selectedImage, isPickerShowing: $isPickerPresented)
                        }
                }else{
                    HStack{
            Image(systemName: "person")
                .resizable()
                .frame(width: 200 , height: 200)
                .padding(32)
                .clipShape(Circle())
                .onTapGesture {
                    isPickerPresented.toggle()
                }.sheet(isPresented: $isPickerPresented){
                    ImagePicker(selectedImage: $selectedImage, isPickerShowing: $isPickerPresented)
                }
                    }.background(Color.gray)
                        .cornerRadius(100)
                    
                    
                }
            }
            
            VStack{
                HStack(){
                    TextField("Name", text: $viewModel.profile.name)
                    Spacer()
                }.padding(10)
                .overlay(
                        RoundedRectangle(cornerRadius: 16)
                            .stroke(.black, lineWidth: 0.5)
                    )
                HStack(){
                    TextField("Suname", text: $viewModel.profile.surname)
                    Spacer()
                }.padding(10)
                .overlay(
                        RoundedRectangle(cornerRadius: 16)
                            .stroke(.black, lineWidth: 0.5)
                    )
                HStack(){
                    Text("+420 ")
                    TextField("Phone", text: $viewModel.profile.number)
                    Spacer()
                }.padding(10)
                .overlay(
                        RoundedRectangle(cornerRadius: 16)
                            .stroke(.black, lineWidth: 0.5)
                    )
                HStack(){
                    TextField("Email", text: $viewModel.profile.email)
                    Spacer()
                }.padding(10)
                .overlay(
                        RoundedRectangle(cornerRadius: 16)
                            .stroke(.black, lineWidth: 0.5)
                    )
            }.padding(10)
        Spacer()
            Button(action: {
                uploadPhoto()
                viewModel.setProfile()
            }) {
                Text("Upload photo")
                    .foregroundColor(.white)
                    .frame(width: 330, height: 20)
            }.padding()
                .background(Color("backround"))
                .cornerRadius(16)
            Spacer()
        }.navigationTitle(Text("Profile"))
        .onSubmit {
            viewModel.setProfile()
            print("On submit")
        }
        .onAppear{
            self.viewModel.getProfile()
            retrivePhotos()
        }
        .background(Color("back"))
    }
    
    func uploadPhoto(){
        
        guard selectedImage != nil else{
            return
        }
        
        let storageRef = Storage.storage().reference()
        let imageData = selectedImage!.jpegData(compressionQuality: 0.8)
        
        guard imageData != nil else {
            return
        }
        let path = "images/\(UUID().uuidString).jpg"
        
        let fileRef = storageRef.child(path)
        
        let uploadTask = fileRef.putData(imageData!, metadata: nil){
            metadata, error in
            
            if error == nil && metadata != nil{
            
               // let db = Firestore.firestore()
               // db.collection("user").document("zbKvum8vP3KlltdaAQ1c").setData(["imageUrl":path])
                viewModel.profile.imageUrl = path
                viewModel.setProfile()
            }
        }
        
    }
    
    func retrivePhotos()  {
        
        let db = Firestore.firestore()
        
        db.collection("user").getDocuments{snapshot, error in
            
            var path: String = ""
            if error == nil && snapshot != nil{
                for doc in snapshot!.documents{
                    
                    path = doc["imageUrl"] as! String
                    
                }
            }
            let storageRef = Storage.storage().reference()
            let fileRef = storageRef.child(path)
            
            fileRef.getData(maxSize: 5*1024*1024) { data, erroe in
                
                
                if error == nil && data != nil{
                    selectedImage = UIImage(data: data!)
                }
            }
            
            
            
        }
    }
}

struct ProfileView_Previews: PreviewProvider {
    static var previews: some View {
        ProfileView(viewModel: ProfileViewModel(profile: LoginProfile(id: "", name: "", surname: "", imageUrl: "", number: "", email: "")))
    }
}
