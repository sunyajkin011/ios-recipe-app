//
//  DetailView.swift
//  Project
//
//  Created by кирилл суняйкин on 18.01.2023.
//

import SwiftUI

struct DetailView: View {
    
    @StateObject var viewModel: DetailViewModel
    @Environment(\.presentationMode) var presentationMode
    
    var body: some View {
        ScrollView{
            AsyncImage(url: URL(string: viewModel.recipe.thumbnailUrl)){ image in
                image
                    .resizable()
                    .aspectRatio(contentMode: .fill)
            } placeholder: {
                Image(systemName: "photo")
                    .resizable()
                    .scaledToFit()
                    .frame(width: 100, height: 100, alignment: .center)
                    .foregroundColor(.white.opacity(0.7))
                    .frame(maxWidth: .infinity, maxHeight: .infinity)
            
            }
            .frame(height: 220)
            .background(LinearGradient(gradient: Gradient(colors: [Color(.gray).opacity(0.3), Color(.gray)]), startPoint: .top, endPoint: .bottom))
            
            VStack(spacing: 60){
                Text(viewModel.recipe.name)
                    .font(.largeTitle)
                    .bold()
                    .multilineTextAlignment(.center)
                if (viewModel.recipe.sections != nil && !viewModel.recipe.sections!.isEmpty){
                    VStack(alignment: .leading, spacing: 20){
                        Text("Ingredients")
                            .font(.headline)
                        ForEach(viewModel.recipe.sections!, id: \.hashValue){ ing in
                            Text(ing)
                        }
                    }
                }
                if (viewModel.recipe.instructions != nil && !viewModel.recipe.instructions!.isEmpty){
                    VStack(alignment: .leading, spacing: 20){
                        Text("Instractions")
                            .font(.headline)
                            
                        ForEach(viewModel.recipe.instructions!, id: \.hashValue){ ins in
                            Text(ins)
                        }
                    }.padding(.horizontal, 10)
                }
                
            }
            
        }
        .ignoresSafeArea(.container, edges: .top)
        .background(Color("back"))
        .toolbar {
                    ToolbarItem {
                        Button(action: {
                            if(self.viewModel.type=="main"){
                                self.viewModel.addToFavoriteRecipe()
                            }else if(self.viewModel.type == "favorite"){
                                self.viewModel.deleteItem()
                                self.presentationMode.wrappedValue.dismiss()
                            }
                        }) {
                            if(self.viewModel.type=="main"){
                                Label("Add Item to favorite", systemImage: "heart")
                            }else if(self.viewModel.type == "favorite"){
                                Label("Add Item to favorite", systemImage: "trash")
                            }
                        }
                    }
                }
        
    }
}

struct DetailView_Previews: PreviewProvider {
    static var previews: some View {
        DetailView(viewModel: DetailViewModel(recipe: RecipeLocal(id: 12, cookTimeMinutes: 10, name: "", instructions: nil, thumbnailUrl: "", sections: nil), type: ""))
    }
}
