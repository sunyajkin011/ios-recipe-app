//
//  LoupeView.swift
//  Project
//
//  Created by кирилл суняйкин on 19.01.2023.
//

import SwiftUI
import PhotosUI
import CoreML
import UIKit
import Vision

struct LoupeView: View {
    
    @State var selectedItems: [PhotosPickerItem] = []
    @State var data: Data?
    @State var isShown = false
    @State var sourceType: UIImagePickerController.SourceType = .camera
    @State var selectedImage: UIImage?
    
    @StateObject var loupeViewModel = LoupeViewModel()
    
    var body: some View {
        VStack {
            if let data = data, let uiImage = UIImage(data: data) {
                Image(uiImage: uiImage)
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(height: 300)

                        Section("Machine identifies this object as:") {
                            ForEach(loupeViewModel.detectedObjects ?? [], id: \.self) { item in
                                NavigationLink{
                                    
                                    let viewModel = AdditionalViewModel( detectedString: item.identifier.capitalized)
                                    
                                    AdditionalPageView(viewModel: viewModel)
                                    
                                }label:{
                                    HStack(){
                                        Text(item.identifier.capitalized)
                                            .font(.headline)
                                            .foregroundColor(.black)
                                            .frame(width: 330, height: 20)
                                    }
                                    .padding()
                                    .background(Color("back"))
                                    .cornerRadius(30)
                                    .padding(12)
                                }
                            }
                        }
            } else {
                Image(systemName: "photo")
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(height: 300)
            }
            
            Picker
        }
        .sheet(isPresented: self.$isShown) {
            ImagePicker(selectedImage: self.$selectedImage, isPickerShowing: self.$isShown)
        }
        
        Spacer()
    }
}

extension LoupeView {
    
    var Picker : some View {
        PhotosPicker(
            selection: $selectedItems,
            maxSelectionCount: 1,
            matching: .images
        ) {
            Text("Pick Photo")
                .font(.system(size: 20, weight: Font.Weight.bold))
                .foregroundColor(Color.white)
                .padding(.horizontal, 20)
                .padding(.vertical, 10)
                .background(RoundedRectangle(cornerRadius: 15).fill(Color.green))
                .frame(width: 300, height: 50)
        }.onChange(of:  selectedItems) { newValue in
            guard let item = selectedItems.first else {
                return
            }
            
            item.loadTransferable(type: Data.self) { result in
                switch result {
                case .success(let data):
                    if let data = data {
                        self.data = data
                        loupeViewModel.detectImage(image: UIImage(data: data)!)
                    } else {
                        print("Data is nil")
                    }
                case .failure(let failure):
                    fatalError("\(failure)")
                }
            }
        }
    }
}

struct LoupeView_Previews: PreviewProvider {
    static var previews: some View {
        LoupeView()
    }
}
