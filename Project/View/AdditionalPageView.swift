//
//  AdditionalPageView.swift
//  Project
//
//  Created by кирилл суняйкин on 23.01.2023.
//

import SwiftUI

struct AdditionalPageView: View {
    
    @StateObject var viewModel: AdditionalViewModel
    @State var isSelected: Bool = false
    @State var isSel: Bool = false
    
    var body: some View {
        ScrollView(.vertical, showsIndicators: false){
            
            ForEach(viewModel.items, id: \.id){item in
                
                HStack(){
                    Text(item.title)
                        .font(.headline)
                    Spacer()
                    Image(systemName:  viewModel.contains(item) ? "checkmark.square.fill" : "checkmark.square")
                        .foregroundColor(.black)
                        .onTapGesture {
                            viewModel.toggleSel(item: item)
                        }
                }
                .padding()
                .background(Color("back"))
                .cornerRadius(30)
                .padding(.horizontal, 12)

                
            }
        }.toolbar {
            ToolbarItem {
                
                NavigationLink{
                    let viewModel = ListViewModel(type: "filter", filterStrings: viewModel.getSelectString())
                    ListView(viewModel: viewModel)
                }label:{
                   Text("Filter")
                    }
            }
        }
        .onAppear{
            viewModel.getAdditionalIngredients()
        }
        .padding(10)
    }
}

struct AdditionalPageView_Previews: PreviewProvider {
    static var previews: some View {
        AdditionalPageView(viewModel: AdditionalViewModel(detectedString: ""))
    }
}
