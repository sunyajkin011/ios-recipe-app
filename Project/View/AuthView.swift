//
//  AuthView.swift
//  Project
//
//  Created by кирилл суняйкин on 18.01.2023.
//

import SwiftUI

struct AuthView: View {
    
    @State private var email = ""
    @State private var password = ""
    
    @State private var isTabViewShow = false
    
    var body: some View {
        
        VStack(spacing: 40){
            Text("Hello")
                .padding(.horizontal, 30)
                .font(.title.bold())
                .foregroundColor(.white)
            Text("Sign in to your account")
                .padding(.horizontal, 30)
                .font(.title2.bold())
                .foregroundColor(.white)
            
            VStack{
                TextField("Enter your email", text: $email)
                    .padding()
                    .background(Color("back"))
                    .cornerRadius(30)
                    .padding(.horizontal, 12)
                    
                SecureField("Enter your password", text: $password)
                    .padding()
                    .background(Color("back"))
                    .cornerRadius(30)
                    .padding(.horizontal, 12)
                
                Button{
                    LoginService.shared.signIn(email: self.email, password: self.password){ result in
                        switch result{
                        case .success(let user):
                            print(user.email!)
                            isTabViewShow.toggle()
                        case .failure(let error):
                            print(error)
                        }
                    }
                }label:{
                    Text("Log in")
                        .frame(maxWidth: .infinity)
                        .padding(.horizontal, 12)
                        .background(Color("back"))
                        .cornerRadius(30)
                        .font(.title2.bold())
                        .foregroundColor(.black)
                }
                .padding(.bottom,10)
                .padding(.top,10)
                Button{
                    LoginService.shared.signUp(email: self.email, password: self.password){ result in
                        switch result{
                        case .success(let user):
                            print(user.email!)
                            isTabViewShow.toggle()
                        case .failure(let error):
                            print(error)
                        }
                    }
                    
                }label:{
                    Text("Register")
                        .frame(maxWidth: .infinity)
                        .padding(.horizontal, 12)
                        .background(Color("back"))
                        .cornerRadius(30)
                        .font(.title2.bold())
                        .foregroundColor(.black)
                }
                    
            }
            
        }
        .padding()
        .padding(.horizontal, 12)
        .cornerRadius(20)
        .background(Color("blackAlpha"))
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .background(Image("bg").ignoresSafeArea())
        .padding()
        .fullScreenCover(isPresented: $isTabViewShow) {
            MainTabView()
        }
    }
}

struct AuthView_Previews: PreviewProvider {
    static var previews: some View {
        AuthView()
    }
}
