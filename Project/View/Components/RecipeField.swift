//
//  RecipeField.swift
//  Project
//
//  Created by кирилл суняйкин on 24.01.2023.
//

import SwiftUI

struct RecipeField: View {
    var recipe: RecipeLocal
    
    var body: some View {
        VStack{
            AsyncImage(url: URL(string: recipe.thumbnailUrl)){ image in
                image
                    .resizable()
                    .aspectRatio(contentMode: .fill)
                    .overlay(alignment: .top){
                        Text(recipe.name)
                            .font(.headline)
                            .foregroundColor(.white)
                            .shadow(color: .black, radius: 3, x: 0, y: 0)
                            .frame(maxWidth:300)
                            .padding()
                    }
            } placeholder: {
                Image(systemName: "photo")
                    .resizable()
                    .scaledToFit()
                    .frame(width: 40, height: 40, alignment: .center)
                    .foregroundColor(.white.opacity(0.7))
                    .frame(maxWidth: .infinity, maxHeight: .infinity)
                    .overlay(alignment: .top){
                        Text(recipe.name)
                            .font(.headline)
                            .foregroundColor(.white)
                            .shadow(color: .black, radius: 3, x: 0, y: 0)
                            .frame(maxWidth:100)
                            .padding()
                    }
                
            }
            
        }
        .frame(width: 350, height: 180, alignment: .top)
        .background(LinearGradient(gradient: Gradient(colors: [Color(.gray).opacity(0.3), Color(.gray)]), startPoint: .top, endPoint: .bottom))
        .clipShape(RoundedRectangle(cornerRadius: 20, style: .continuous))
    }
}

struct RecipeField_Previews: PreviewProvider {
    static var previews: some View {
        RecipeField(recipe: RecipeLocal(id: 1, cookTimeMinutes: 100, name: "recipe", instructions: [], thumbnailUrl: "", sections: []))
    }
}
