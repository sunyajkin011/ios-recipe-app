//
//  TabView.swift
//  Project
//
//  Created by кирилл суняйкин on 19.01.2023.
//

import SwiftUI

struct MainTabView: View {
    var body: some View {
        
        TabView{
            NavigationView{
                ListView(viewModel: ListViewModel(type: "main"))
            }
                .tabItem {
                        Image(systemName: "list.bullet")
                }
            
            NavigationView{
                ListView(viewModel: ListViewModel(type: "favorite"))
            }
                .tabItem {
                    
                        Image(systemName: "heart")
                    
                }
            
            NavigationView{
                LoupeView()
            }
                .tabItem {
                    
                        Image(systemName: "magnifyingglass")
                    
                }
            
            NavigationView{
                ProfileView(viewModel: ProfileViewModel(profile: LoginProfile(id: "", name: "", surname: "", imageUrl: "", number: "", email: "")))
            }
                .tabItem {
                    
                        Image(systemName: "person.crop.circle")
                    
                }
        }
    }
}

struct TabView_Previews: PreviewProvider {
    static var previews: some View {
        MainTabView()
    }
}
