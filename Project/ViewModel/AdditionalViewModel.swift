//
//  AdditionalPageViewModel.swift
//  Project
//
//  Created by кирилл суняйкин on 23.01.2023.
//

import Foundation


class AdditionalViewModel: ObservableObject {
    
    @Published var detectedString: String
    @Published var additionalIngredients: [String]
    @Published var items: [FilterItem]
    
    @Published var savedItems: Set<Int> = []
    
    @Published var finalList: [String]
    
    private var db = Database()
    
    
    init(detectedString: String){
        self.detectedString = detectedString
        self.additionalIngredients = [String]()
        self.finalList = [String]()
        self.items = [FilterItem]()
    }
    
    func contains(_ item: FilterItem) -> Bool{
        savedItems.contains(item.id)
    }
    
    func toggleSel(item: FilterItem){
        if(contains(item)){
            savedItems.remove(item.id)
        }else{
            savedItems.insert(item.id)
        }
    }
    
    func makeItems(list: [String]){
        var i = 0
        for item in list{
            items.append(FilterItem(id: i, title: item, isSelected: false))
            i += 1
        }
    }
    
    func getSelectString()->[String]{
        var selStr = [String]()
        
        for tm in self.items{
            if(self.contains(tm)){
                selStr.append(tm.title)
            }
        }
        return selStr
        
    }
    
    func getAdditionalIngredients(){
        var rec: [RecipeLocal] = []
        FirebaseService.shared.getAllLocalRecipes{ result in
            switch result{
            case .success(let recipes):
                rec = recipes
                print("Count of recipies: \(recipes.count)")
                for r in rec{
                    print(self.isRecTrue(rec: r))
                    if(self.isRecTrue(rec: r)){
                        for s in r.sections!{
                            self.additionalIngredients.append(s)
                        }
                    }
                }
                self.makeItems(list: self.additionalIngredients)
            case .failure(let error):
                print (error.localizedDescription)
            }
        }
    }
    
    func isRecTrue(rec: RecipeLocal)->Bool{
        if(getString(rec: rec) != nil && (getString(rec: rec)!.contains(self.detectedString.lowercased()))){
            return true
        }else{
            return false
        }
            
    }
    
    func getString(rec: RecipeLocal)->String?{
        var finalStr: String = ""
        if(rec.sections != nil){
            for str in rec.sections!{
                finalStr += str
            }
            return finalStr
        }else{
            return nil
        }
    }
    
    
}
