//
//  ProfileViewModel.swift
//  Project
//
//  Created by кирилл суняйкин on 21.01.2023.
//

import Foundation

class ProfileViewModel: ObservableObject{
    
    @Published var profile : LoginProfile
    
    init(profile: LoginProfile){
        self.profile = profile
    }
    
    
    func setProfile(){
        
        FirebaseService.shared.setUser(user: self.profile) {
            result in
        
            switch result {
            case .success(let user):
                print (user.name)
        case .failure(let error):
            print ("Send error \(error.localizedDescription)")
            }
        }
    }
    
    
    func getProfile(){
        FirebaseService.shared.getUser{result in
            switch result{
            case .success(let user):
                self.profile = user
                print("User: \(self.profile.name)")
            case .failure(let error):
                print (error.localizedDescription)
            }
            }
        }
    
}
