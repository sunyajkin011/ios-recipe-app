//
//  ListViewModel.swift
//  Project
//
//  Created by кирилл суняйкин on 19.01.2023.
//

import Foundation


class ListViewModel: ObservableObject {
    
    @Published var type: String
    @Published var recipes: [RecipeLocal]
    @Published var filterStrings: [String]?
        
    init(type: String) {
        self.recipes = [RecipeLocal]()
        self.type = type
    }
    init(type: String, filterStrings: [String]) {
        self.recipes = [RecipeLocal]()
        self.type = type
        self.filterStrings = filterStrings
    }
    
    func fetchData(){
        if(self.type == "main"){
            ApiService().getAllRecipies {[weak self] (result) in
                
                switch result {
                case .success(let data):
                    self?.recipes = self!.toLocalRecipe(resIn: data.results)
                    self!.setAllLocalRecipe()
                case .failure( let error):
                    print(error)
                }
            }
        }else if(self.type == "favorite"){
            FirebaseService.shared.getFavoriteRecipes{ result in
                switch result{
                case .success(let recipes):
                    self.recipes = self.convertFavoriteRecipeToLocalRecipe(resipes: recipes)
                    print("Count of recipies: \(recipes.count)")
                case .failure(let error):
                    print (error.localizedDescription)
                }
            }
        }
        else if(self.type == "filter"){
            FirebaseService.shared.getAllLocalRecipes{ result in
                switch result{
                case .success(let recipes):
                    print("Count of recipies: \(recipes.count)")
                    for r in recipes{
                        print(self.isRecTrue(rec: r))
                        if(self.isRecTrue(rec: r)){
                                self.recipes.append(r)
                        }
                    }
                case .failure(let error):
                    print (error.localizedDescription)
                }
            }
        }
        
    }
    
    func isRecTrue(rec: RecipeLocal)->Bool{
        var i=0
        if(self.filterStrings != nil){
            for str in self.filterStrings!{
                if(getString(rec: rec) != nil && (getString(rec: rec)!.contains(str.lowercased()))){
                    i += 1
                }
            }
            if(i>1){
                return true
            }else{
                return false
            }
                
        }else{
            return false
        }
        
    }
    
    func getString(rec: RecipeLocal)->String?{
        var finalStr: String = ""
        if(rec.sections != nil){
            for str in rec.sections!{
                finalStr += str
            }
            return finalStr
        }else{
            return nil
        }
        
    }
    
    
    func convertFavoriteRecipeToLocalRecipe(resipes: [FavoriteRecipe])->[RecipeLocal]{
        var newResList: [RecipeLocal] = []
        
        for res in resipes{
            let r = RecipeLocal(id: res.id, cookTimeMinutes: res.cookTimeMinutes, name: res.name, instructions: res.instractions, thumbnailUrl: res.thumbnailUrl, sections: res.sections)
            newResList.append(r)
        }
        return newResList
        }
    

        
        func setAllLocalRecipe(){
            FirebaseService.shared.setAllLocalRecipe(recipesLocal: self.recipes){
                result in
                switch result{
                case .success(let localRecipe):
                    print(localRecipe.name)
                case .failure(let error):
                    print(error.localizedDescription)
                }
                
            }
        }
    
        
func toLocalRecipe(resIn: [Recipe])->[RecipeLocal]{
            var newLocalList: [RecipeLocal] = []
            
            for res in resIn{
                var loc = RecipeLocal(id: res.id, cookTimeMinutes: res.cook_time_minutes, name: res.name, instructions: toString1(ins: res.instructions), thumbnailUrl: res.thumbnail_url, sections: toString2(sec: res.sections))
                newLocalList.append(loc)
            }
            return newLocalList
            
        }
    
    func toString1(ins: [Instruction]?)->[String]?{
        if(ins != nil){
            var ls:[String] = []
            for str in ins!{
                ls.append(str.display_text)
            }
            return ls
        }else{
            return nil
        }
        
    }
        
    func toString2(sec: [Position]?)->[String]?{
        if(sec != nil){
            var ls:[String] = []
            for str in sec!{
                
                ls.append(contentsOf: (toString3(com: str.components)))
            }
            return ls
        }else{
            return nil
        }
    }
    
    func toString3(com: [Component])->[String]{
            var ls:[String] = []
            for str in com{
                ls.append(str.raw_text)
            }
            return ls
        
    }
}
