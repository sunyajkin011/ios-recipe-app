//
//  DetailViewModel.swift
//  Project
//
//  Created by кирилл суняйкин on 19.01.2023.
//

import Foundation

class DetailViewModel: ObservableObject {
    
    @Published var type: String
    
    @Published var recipe: RecipeLocal

    
    init(recipe: RecipeLocal, type: String){
        self.recipe = recipe
        self.type = type
    }
    
    
    func addToFavoriteRecipe(){
        FirebaseService.shared.setFavoriteRecipe(recipeFavorite: FavoriteRecipe(id: self.recipe.id, userId: LoginService.shared.currentUser!.uid, cookTimeMinutes: self.recipe.cookTimeMinutes ?? 0, name: self.recipe.name, instractions: self.recipe.instructions, thumbnailUrl: self.recipe.thumbnailUrl, sections: self.recipe.sections ?? [])){
            result in
            switch result{
            case .success(let recipe):
                print(recipe.name)
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
     
    }
    func deleteItem(){
            FirebaseService.shared.deleteItem(item: FavoriteRecipe(id: self.recipe.id, userId: LoginService.shared.currentUser!.uid, cookTimeMinutes: self.recipe.cookTimeMinutes ?? 0, name: self.recipe.name, instractions: self.recipe.instructions, thumbnailUrl: self.recipe.thumbnailUrl, sections: self.recipe.sections ?? []))
        }
    
    
}
    
