//
//  LoupeViewModel.swift
//  Project
//
//  Created by кирилл суняйкин on 19.01.2023.
//

import Foundation
import CoreML
import Vision
import UIKit
import Combine


class LoupeViewModel: ObservableObject {
    
    @Published var detectedObjects: [VNClassificationObservation]? = nil
    
    func detectImage(image: UIImage) {
        guard let model = try? VNCoreMLModel(for: Resnet50().model) else {
            fatalError("Cannot load ML model")
        }
        
        let request = VNCoreMLRequest(model: model) {  request, error in
            guard let results = request.results as? [VNClassificationObservation], let _ = results.first else {
                fatalError("Cannot get result from VNCoreMLRequest")
            }
            
            DispatchQueue.main.async {
                self.detectedObjects = results .filter { $0.confidence > 0.1 }
            }
        }
        
        guard let ciImage = CIImage(image:image) else {
            fatalError("Cannot convert to CIImage")
        }
                
        DispatchQueue.global(qos: .userInteractive).async {
            do {
                try VNImageRequestHandler(ciImage: ciImage).perform([request])
            } catch {
                print("Error \(error)")
            }
        }
    }
}
